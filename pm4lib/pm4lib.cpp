// pm4lib.cpp : Definiert die exportierten Funktionen f�r die DLL-Anwendung.
//

#include "stdafx.h"
#include <jni.h>
#include "pm4.h"
#include "pm4lib.h"

#include <PM3USBCP.h>
#include <PM3DDICP.h>
#include <PM3CsafeCP.h>


void debug(const char* msg);
void error(const char* msg);
void errorCode(ERRCODE_T ecode);
UINT16_T discoverDevices();


JNIEXPORT jshort JNICALL Java_org_wmmnpr_c2_PmLib_tkcmdsetDDI_1init(JNIEnv *env, jclass clzPmLib)
{
	debug("Java_org_wmmnpr_c2_PmLib_tkcmdsetDDI_1init - start");
	ERRCODE_T ecode = tkcmdsetDDI_init();

	if(ecode != 0){
		errorCode(ecode);
	}

    UINT16_T num_units = discoverDevices();

	char rspbuf[1024] = {0x00};
    if (num_units){
        sprintf(rspbuf,"PM's discovered: %u",num_units);
    }else{
        sprintf(rspbuf,"No PM's discovered");
    }
	debug(rspbuf);

	debug("Java_org_wmmnpr_c2_PmLib_tkcmdsetDDI_1init - end");
	return ecode;
}

JNIEXPORT jshort JNICALL Java_org_wmmnpr_c2_PmLib_tkcmdsetCSAFE_1init_1protocol(JNIEnv *, jclass, jint timeout)
{
	debug("Java_org_wmmnpr_c2_PmLib_tkcmdsetCSAFE_1init_1protocol - start");
    ERRCODE_T ecode = tkcmdsetCSAFE_init_protocol((UINT16_T)timeout);
	if(ecode != 0){
		errorCode(ecode);
	}
	debug("Java_org_wmmnpr_c2_PmLib_tkcmdsetCSAFE_1init_1protocol - end");
	return ecode;
}

JNIEXPORT jshort JNICALL Java_org_wmmnpr_c2_PmLib_tkcmdsetDDI_1discover_1PMs(JNIEnv *, jclass, jobject){
	  return -1;
}

JNIEXPORT jshort JNICALL Java_org_wmmnpr_c2_PmLib_tkcmdsetCSAFE_1command(JNIEnv *env, jclass clzPmLib, jint unit, jobject inIntBuffer, jobject outIntBuffer)
{
	debug("Java_org_wmmnpr_c2_PmLib_tkcmdsetCSAFE_1command - start");
	UINT16_T unit_address = unit;
    UINT16_T cmd_data_size = 0;
	UINT32_T cmd_data[64] = {0x00};
    UINT16_T rsp_data_size = 100;
	UINT32_T rsp_data[64] = {0x00};
	ERRCODE_T ecode;

   jclass clz = env->GetObjectClass(inIntBuffer); 
   jmethodID mid = env->GetMethodID(clz, "limit", "()I");
   if(mid == NULL) return -1;

   jint inLen = env->CallIntMethod(inIntBuffer, mid);
   if(inLen < 0) return -1;
   printf("inbuffer length: %d\n", inLen); 
  

   unsigned long *sourcePtr = (unsigned long*)env->GetDirectBufferAddress(inIntBuffer);
   unsigned long *destPtr = (unsigned long*)env->GetDirectBufferAddress(outIntBuffer);
   printf("inbuffer address: %p\n", sourcePtr);

   int idx;
   for(idx=0; idx<inLen; idx++){
	   cmd_data[idx] = (UINT32_T) sourcePtr[idx];
	   cmd_data[idx] = cmd_data[idx] >> (sizeof(UINT32_T)*8 - 8);
	   printf("%x:", cmd_data[idx]);
	   cmd_data_size++;
   }

	rsp_data_size = 64;
	memset(rsp_data, 0x00, 64);

	ecode = tkcmdsetCSAFE_command(unit_address, cmd_data_size, cmd_data, &rsp_data_size, rsp_data);
	if(ecode != 0){
		errorCode(ecode);
		return ecode;
	}

	/*
	jint *values = env->GetIntArrayElements(jrsp_data, NULL);
	values[0] = rsp_data[0];
	values[1] = rsp_data[1];
	values[2] = rsp_data[2];
	values[3] = rsp_data[3];
	*/
	debug("Java_org_wmmnpr_c2_PmLib_tkcmdsetCSAFE_1command - end");
	return ecode;
}

JNIEXPORT jshort JNICALL Java_org_wmmnpr_c2_PmLib_tkcmdsetDDI_1serial_1number(JNIEnv *env, jclass clzPmLib, jint unit, jobject outByteBuffer)
{
	char *destPtr = (char*)env->GetDirectBufferAddress(outByteBuffer);
	const char*sn = "DE112358";

	//tkcmdsetDDI_serial_number

	destPtr[0]='D';
	destPtr[1]='E';
	destPtr[2]='0';
	destPtr[3]='1';
	//sprintf_s(destPtr, strlen(sn), "%s", sn);

	return 0;
}

void debug(const char* msg){
	printf("DEBUG: %s\n", msg);
}

void error(const char* msg){
	printf("ERROR: %s\n", msg);
}

void errorCode(ERRCODE_T ecode){
	char name[40];
	char text[200];
	char memo[250];

	// Format the error by looking up info in INI file
	tkcmdsetDDI_get_error_name(ecode,name,sizeof(name));
	tkcmdsetDDI_get_error_text(ecode,text,sizeof(text));
      
	sprintf_s(memo,"Error (%d):  %s\r\n%s",ecode,name,text);
	error(memo);
}


UINT16_T discoverDevices()
{
   UINT16_T i;
   UINT16_T j;
   UINT16_T numOldPM3Devices = 0;
   UINT16_T numNewPM3Devices = 0;
   UINT16_T numPM4Devices = 0;
   ERRCODE_T ecode = 0;
      
   UINT16_T numCommunicating = 0;

   // Look for PM3 devices
   ecode = tkcmdsetDDI_discover_pm3s(TKCMDSET_PM3_PRODUCT_NAME2, 0, &numCommunicating);
  
   if (!ecode && numCommunicating)
   {
      // We discovered one or more PM3's
      numNewPM3Devices = numCommunicating;
   }

   // Look for old style PM3 devices, starting numbering after the previous
   ecode = tkcmdsetDDI_discover_pm3s(TKCMDSET_PM3_PRODUCT_NAME, numCommunicating, &numCommunicating);
  
   if (!ecode && numCommunicating)
   {
      // We discovered one or more old PM3's
      numOldPM3Devices = numCommunicating - numNewPM3Devices;
   }

   // Look for PM4 devices
   ecode = tkcmdsetDDI_discover_pm3s(TKCMDSET_PM4_PRODUCT_NAME, numCommunicating, &numCommunicating);
   if (!ecode && numCommunicating)
   {
      // We discovered one or more PM4's
      numPM4Devices = numCommunicating - numNewPM3Devices - numOldPM3Devices;
   }

   // Initialize each of the PMs discovered
   j = numCommunicating;
   for (i=0; i<j; i++)
   {

   }

   return numCommunicating;
}
